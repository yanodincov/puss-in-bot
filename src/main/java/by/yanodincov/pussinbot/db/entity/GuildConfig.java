package by.yanodincov.pussinbot.db.entity;

import jakarta.annotation.Nullable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;

@Entity
@Table(name = "guild_config")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GuildConfig {
    @Id
    @Column(name = "guild_id")
    private Long guildID;

    @Nullable
    @Column(name = "control_role_id", nullable = true)
    private Long controlRoleID;

    @Nullable
    @Column(name = "default_member_role", nullable = true)
    private Long defaultMemberRoleID;

    @Nullable
    @Column(name = "default_slots", nullable = true)
    private Integer defaultSlots;

    @Nullable
    @Column(name = "default_announce_channel_id", nullable = true)
    private Long defaultAnnounceChannelID;

    @Nullable
    @Column(name = "default_log_channel_id", nullable = true)
    private Long defaultLogChannelID;

    @Nullable
    @Column(name = "default_chips_set_price", nullable = true)
    private Integer defaultChipsSetPrice;

    @Nullable
    @Column(name = "default_chips_set_size", nullable = true)
    private Integer defaultChipsSetSize;

    public GuildConfig(Long guildID) {
        this.guildID = guildID;
    }
}
