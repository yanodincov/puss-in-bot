package by.yanodincov.pussinbot.db.entity;


import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "game_member", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"game_id", "member_id"})
})

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GameMember {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne()
    @JoinColumn(name = "game_id")
    private Game game;

    @ManyToOne()
    @JoinColumn(name = "member_id")
    private Member member;

    @Column(name = "chips_bought", columnDefinition = "integer default 0")
    private Integer chipsBought;

    @Column(name = "chips_rest", columnDefinition = "integer default 0")
    private Integer chipsRest;
}
