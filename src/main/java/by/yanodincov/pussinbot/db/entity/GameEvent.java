package by.yanodincov.pussinbot.db.entity;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "game_event")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GameEvent {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @OneToOne()
    @JoinColumn(name = "game_id", nullable = false, referencedColumnName = "id")
    private Game game;

    @Column(name = "is_started", columnDefinition = "boolean not null default false")
    private Boolean isStarted = false;

    @Column(name = "is_stopped", columnDefinition = "boolean not null default false")
    private Boolean isStopped = false;

    @Column(name = "is_delete_announce_leave_button", columnDefinition = "boolean not null default false")
    private Boolean isDeletedAnnounceLeaveButton = false;

    public GameEvent(Game game) {
        this.game = game;
    }
}
