package by.yanodincov.pussinbot.db.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Table(name = "member")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Member {
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "games_count", columnDefinition = "integer default 0")
    private Integer gamesCount;

    @Column(name = "total_income", columnDefinition = "integer default 0")
    private Integer totalIncome;

    @OneToMany(mappedBy = "member", cascade = CascadeType.ALL)
    private List<GameMember> gameMembers;

    public Member(Long id) {
        this.id = id;
    }
}
