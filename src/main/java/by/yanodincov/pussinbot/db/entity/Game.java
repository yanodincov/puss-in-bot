package by.yanodincov.pussinbot.db.entity;

import jakarta.persistence.*;
import lombok.*;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "game")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Game {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "emoji_code", unique = true)
    private String emojiCode;

    @Column(name = "guild_id", nullable = false)
    private Long guildID;

    @Column(name = "created_at", nullable = false)
    private ZonedDateTime createdAt;

    @Column(name = "start_at", nullable = false)
    private ZonedDateTime startAt;

    @Column(name = "member_role", nullable = false)
    private Long memberRole;

    @Column(name = "log_channel_id", nullable = false)
    private Long logChannelID;

    @Column(name = "announce_channel_id", nullable = false)
    private Long announceChannelID;

    @Column(name = "slots", nullable = false)
    private Integer slots;

    @Column(name = "chips_set_price", nullable = false)
    private Integer chipsSetPrice;

    @Column(name = "chips_set_size", nullable = false)
    private Integer chipsSetSize;

    @Column(name = "announce_message_id")
    private Long announceMessageID;

    @OneToOne(mappedBy = "game", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private GameEvent event;

    @OneToMany(mappedBy = "game", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<GameMember> gameMembers = new ArrayList<>();
}
