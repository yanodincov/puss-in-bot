package by.yanodincov.pussinbot.db.repository;

import by.yanodincov.pussinbot.db.entity.Game;
import by.yanodincov.pussinbot.db.entity.GameEvent;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface GameEventRepository extends CrudRepository<GameEvent, Long> {
    @Transactional
    @Modifying
    @Query("update GameEvent g set g.isStopped = ?2 where g.game = ?1")
    void UpdateEventIsStopped(Game game, Boolean isStopped);

    @Transactional
    @Modifying
    @Query("update GameEvent g set g.isStarted = ?2 where g.game = ?1")
    void UpdateEventIsStarted(Game game, Boolean isStarted);
}
