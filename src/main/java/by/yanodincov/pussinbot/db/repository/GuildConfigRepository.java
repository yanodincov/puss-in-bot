package by.yanodincov.pussinbot.db.repository;

import by.yanodincov.pussinbot.db.entity.GuildConfig;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GuildConfigRepository extends CrudRepository<GuildConfig, Long> {
}
