package by.yanodincov.pussinbot.db.repository;

import by.yanodincov.pussinbot.db.entity.Member;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberRepository extends CrudRepository<Member, Long> {
}
