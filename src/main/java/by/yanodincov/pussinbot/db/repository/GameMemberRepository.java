package by.yanodincov.pussinbot.db.repository;

import by.yanodincov.pussinbot.db.entity.GameMember;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

@Repository
public interface GameMemberRepository extends CrudRepository<GameMember, Long> {
    GameMember findByGame_IdAndMember_Id(@NonNull Long id, @NonNull Long id1);

    @Query("select gm from GameMember gm where gm.game.id=:game_id and gm.member.id=:member_id")
    GameMember getByGameIDAndMemberID(@Param("game_id") Long gameID, @Param("member_id") Long memberID);
}
