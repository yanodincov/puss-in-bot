package by.yanodincov.pussinbot.db.repository;

import by.yanodincov.pussinbot.db.entity.Game;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;

@Repository
public interface GameRepository extends CrudRepository<Game, Long> {
    List<Game> findByEvent_IsStartedFalseAndStartAtLessThan(ZonedDateTime startAt);

    @org.springframework.transaction.annotation.Transactional
    @Modifying
    @Query("update Game g set g.announceMessageID = ?2 where g.id = ?1")
    void updateAnnounceMessageIDById(@NonNull Long gameID, @NonNull Long announceMessageID);

    List<Game> findByAnnounceMessageIDNullAndEvent_IsStoppedFalse();

    @Modifying
    @Transactional
    @Query("delete Game g where g.id in (?1)")
    void deleteByIds(List<Long> ids);
}
