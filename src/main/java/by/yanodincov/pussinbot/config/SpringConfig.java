package by.yanodincov.pussinbot.config;

import by.yanodincov.pussinbot.bot.button.jda.ButtonFactory;
import by.yanodincov.pussinbot.bot.command.jda.CommandFactory;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDARequestField;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDARequestFieldParser;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDARequestFieldParserQualifier;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDARequestHandler;
import by.yanodincov.pussinbot.bot.jda.request.parser.AbstractRequestFieldParser;
import by.yanodincov.pussinbot.bot.jda.request.parser.provider.RequestFieldParserProvider;
import jakarta.annotation.PostConstruct;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.interactions.callbacks.IReplyCallback;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.lang.annotation.Annotation;
import java.util.*;

@Configuration
@EnableTransactionManagement
@EnableJpaAuditing
@EnableScheduling
@ComponentScan(basePackages = {"by.yanodincov.pussinbot"})
@EntityScan(basePackages = {"by.yanodincov.pussinbot.db.entity"})
@EnableJpaRepositories(basePackages = {"by.yanodincov.pussinbot.db.repository"})
@PropertySource("classpath:application.properties")
public class SpringConfig {
    @PostConstruct
    public void init() {
        TimeZone.setDefault(TimeZone.getTimeZone("Europe/Moscow"));
        Locale.setDefault(Locale.of("ru"));
    }

    @Bean
    @Autowired
    public JDA jda(
            @Value("${discord.Token}") String token,
            CommandFactory commandFactory,
            ButtonFactory buttonFactory
    ) {
        var jda = JDABuilder.
                createDefault(token).
                addEventListeners(commandFactory.createListener(), buttonFactory.createListener()).
                build();
        jda.updateCommands().
                addCommands(commandFactory.createCommands()).
                queue();
        jda.addEventListener();

        return jda;
    }

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Bean
    @Autowired
    public <E extends IReplyCallback> RequestFieldParserProvider<E> requestFieldParserProvider(@NotNull @JDARequestFieldParserQualifier List<AbstractRequestFieldParser<E>> parsers) {
        var parsersMap = new HashMap<Class<? extends Annotation>, Map<Class<? extends Annotation>, AbstractRequestFieldParser<E>>>();
        parsers.forEach(p -> {
            var annotation = p.getClass().getAnnotation(JDARequestFieldParser.class);
            if (annotation == null) {
                throw new IllegalArgumentException("Usage JDARequestFieldParserQualifier forbidden - you must use JDARequestFieldParser (Class - %s)".
                        formatted(p.getClass()));
            }
            if (AnnotationUtils.getAnnotation(annotation.fieldAnnotation(), JDARequestField.class) == null) {
                throw new IllegalArgumentException("Class %s annotation JDARequestFieldParser::fieldAnnotation() must contains JDARequestField annotation".
                        formatted(p.getClass()));
            }
            if (AnnotationUtils.getAnnotation(annotation.requestHandlerAnnotation(), JDARequestHandler.class) == null) {
                throw new IllegalArgumentException("Class %s annotation JDARequestFieldParser::requestHandlerAnnotation() must contains JDARequestHandler annotation".
                        formatted(p.getClass()));
            }

            if (!parsersMap.containsKey(annotation.requestHandlerAnnotation())) {
                parsersMap.put(annotation.requestHandlerAnnotation(), new HashMap<>());
            }

            parsersMap.get(annotation.requestHandlerAnnotation()).put(annotation.fieldAnnotation(), p);
        });

        return new RequestFieldParserProvider<>(parsersMap);
    }
}
