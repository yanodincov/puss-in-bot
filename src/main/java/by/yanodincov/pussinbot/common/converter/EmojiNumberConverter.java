package by.yanodincov.pussinbot.common.converter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class EmojiNumberConverter {
    private final Map<Character, String> emojisByInt;

    public EmojiNumberConverter(@Value("${emoji.numbers}") String[] emojiNumbers) {
        this.emojisByInt = new HashMap<>(9);
        for (int i = 0; i < emojiNumbers.length; i++) {
            this.emojisByInt.put((char) (i + '0'), emojiNumbers[i]);
        }
    }

    public String createEmojiNumber(Integer number) {
        List<String> emojisNumber = new ArrayList<>();
        for (char c : number.toString().toCharArray()) {
            emojisNumber.add(this.emojisByInt.get(c));
        }

        return String.join(" ", emojisNumber);
    }
}
