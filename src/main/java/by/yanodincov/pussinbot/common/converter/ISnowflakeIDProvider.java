package by.yanodincov.pussinbot.common.converter;

import net.dv8tion.jda.api.entities.ISnowflake;

public class ISnowflakeIDProvider {
    public static Long getID(ISnowflake entity) {
        if (entity == null) {
            return null;
        }
        return entity.getIdLong();
    }
}
