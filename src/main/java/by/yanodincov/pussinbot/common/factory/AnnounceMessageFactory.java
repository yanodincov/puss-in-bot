package by.yanodincov.pussinbot.common.factory;

import by.yanodincov.pussinbot.bot.button.factory.ButtonRoute;
import by.yanodincov.pussinbot.common.converter.EmojiNumberConverter;
import by.yanodincov.pussinbot.db.entity.Game;
import by.yanodincov.pussinbot.db.entity.GameMember;
import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.entities.emoji.Emoji;
import net.dv8tion.jda.api.interactions.components.ItemComponent;
import net.dv8tion.jda.api.interactions.components.buttons.Button;
import net.dv8tion.jda.api.utils.messages.MessageCreateBuilder;
import net.dv8tion.jda.api.utils.messages.MessageCreateData;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Locale;

@Component
@AllArgsConstructor
public class AnnounceMessageFactory {
    private static final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("EEEE dd MMMM в HH:mm", Locale.getDefault());
    private final EmojiNumberConverter emojiNumberFactory;

    public MessageCreateData createMessageData(Game game) {
        var buttons = new ArrayList<ItemComponent>();
        if (game.getGameMembers().size() < game.getSlots()) {
            buttons.add(Button.success(
                    ButtonRoute.createName(ButtonRoute.Button.JOIN_GAME, game.getId()),
                    "Запишите меня"
            ).withEmoji(Emoji.fromUnicode("U+1F90D")));
        }

        if (!game.getEvent().getIsStarted()) {
            buttons.add(Button.secondary(
                    ButtonRoute.createName(ButtonRoute.Button.LEAVE_GAME, game.getId()),
                    "Я сливаюсь"
            ).withEmoji(Emoji.fromUnicode("U+1F494")));
        }

        return new MessageCreateBuilder().
                addContent(this.createText(game)).
                addActionRow(buttons).
                build();
    }

    public String createText(Game game) {
        StringBuilder members = new StringBuilder();
        if (game.getGameMembers().size() > 0) {
            members.append("Играют:\n");
            for (GameMember gameMember : game.getGameMembers()) {
                members.append("* <@%d>\n".formatted(gameMember.getMember().getId()));
            }
        }

        var template = """
                Анонс новой игры
                :emoji_code

                Играют:   <@&:member_role_id>
                Когда:   :calendar_spiral: :start_at
                Ставки:   :red_circle: :chips_set_size фишек  =  :coin: :chips_set_price рублей
                Учатсников:   :slots_used  /  :slots
                                
                :members
                """;

        return template.
                replace(":emoji_code", game.getEmojiCode()).
                replace(":member_role_id", game.getMemberRole().toString()).
                replace(":start_at", game.getStartAt().format(dateFormat)).
                replace(":chips_set_price", game.getChipsSetPrice().toString()).
                replace(":chips_set_size", game.getChipsSetSize().toString()).
                replace(":slots_used", this.emojiNumberFactory.createEmojiNumber(game.getGameMembers().size())).
                replace(":slots", this.emojiNumberFactory.createEmojiNumber(game.getSlots())).
                replace(":members", members.toString());
    }
}
