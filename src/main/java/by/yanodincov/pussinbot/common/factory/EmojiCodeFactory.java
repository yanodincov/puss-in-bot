package by.yanodincov.pussinbot.common.factory;

import org.apache.commons.collections4.set.ListOrderedSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class EmojiCodeFactory {
    private static final int maxAttmpts = 3;
    private final String[] emojis;
    private final Random randomGenerator = new Random();

    @Autowired
    public EmojiCodeFactory(
            @Value("${emoji.list}") String[] emojis
    ) {
        this.emojis = emojis;
    }


    public String createEmojiCode(Integer count) {
        var emojis = new ListOrderedSet<String>();
        while (emojis.size() < count) {
            emojis.add(
                    this.emojis[this.randomGenerator.nextInt(this.emojis.length)]
            );
        }

        return String.join(" ", emojis);
    }
}
