package by.yanodincov.pussinbot.common.factory;

public class ErrorMessageFactory {
    public static String createErrorMessage(String message) {
        return ":warning: Есть проблемка: %s".formatted(message);
    }
}
