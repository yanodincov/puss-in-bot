package by.yanodincov.pussinbot.bot.jda.request.consumer.handler;

import by.yanodincov.pussinbot.common.factory.ErrorMessageFactory;
import net.dv8tion.jda.api.interactions.callbacks.IReplyCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class ExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(ExceptionHandler.class);

    public void logAndHandle(IReplyCallback event, String errorMessage, Object... args) {
        errorMessage = errorMessage.formatted(args);
        logger.error(errorMessage);
        handle(event, errorMessage);
    }

    public void handle(IReplyCallback event, String errorMessage, Object... args) {
        Objects.requireNonNull(event.getMember()).
                getUser().
                openPrivateChannel().
                complete().
                sendMessage(ErrorMessageFactory.createErrorMessage(errorMessage.formatted(args))).
                queue();
    }
}
