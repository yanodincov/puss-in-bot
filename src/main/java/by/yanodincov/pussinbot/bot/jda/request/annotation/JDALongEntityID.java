package by.yanodincov.pussinbot.bot.jda.request.annotation;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface JDALongEntityID {
    // Return entity LongID, if option object type
    // instance of ISnowflake interface
}
