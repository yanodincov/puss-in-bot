package by.yanodincov.pussinbot.bot.jda.request.parser;

import jakarta.annotation.Nonnull;
import jakarta.validation.ValidationException;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.interactions.callbacks.IReplyCallback;

import java.lang.reflect.Field;

public abstract class AbstractRequestJDAParser<E extends IReplyCallback> extends AbstractRequestFieldParser<E> {
    @Override
    public void validateField(@Nonnull Field field) throws ValidationException {
        if (!field.getType().isAssignableFrom(JDA.class)) {
            throw new ValidationException("must instance of %s".formatted(JDA.class.getName()));
        }
    }
}
