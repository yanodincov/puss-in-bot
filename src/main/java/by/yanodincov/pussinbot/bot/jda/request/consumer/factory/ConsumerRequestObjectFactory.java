package by.yanodincov.pussinbot.bot.jda.request.consumer.factory;

import by.yanodincov.pussinbot.bot.jda.exception.ValidationException;
import by.yanodincov.pussinbot.bot.jda.request.RequestFactory;
import jakarta.annotation.Nonnull;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.interactions.callbacks.IReplyCallback;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.SmartValidator;

import java.lang.reflect.Method;
import java.util.Objects;

@Component
@AllArgsConstructor
public class ConsumerRequestObjectFactory<E extends IReplyCallback> {

    protected SmartValidator validator;
    protected RequestFactory<E> requestFactory;

    public Object createRequest(@Nonnull Method method, @Nonnull E event) throws ReflectiveOperationException, ValidationException {
        if (method.getParameters().length < 1) {
            return null;
        }
        var parameter = method.getParameters()[0];
        var request = this.requestFactory.fillRequest(
                method,
                parameter.getType().getDeclaredConstructor().newInstance(),
                event
        );
        if (parameter.isAnnotationPresent(Valid.class)) {
            Errors errors = new BeanPropertyBindingResult(request, "commandRequest");
            this.validator.validate(request, errors);

            if (errors.hasFieldErrors()) {
                throw new ValidationException(Objects.requireNonNull(errors.getFieldError()).getDefaultMessage());
            }
        }

        return request;
    }
}
