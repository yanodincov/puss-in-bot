package by.yanodincov.pussinbot.bot.jda.validation;

import by.yanodincov.pussinbot.bot.jda.validation.validator.ChannelHasTypeValidator;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import net.dv8tion.jda.api.entities.channel.ChannelType;

import java.lang.annotation.*;

@Documented
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ChannelHasTypeValidator.class)
public @interface ChannelHasType {
    String message() default "Invalid channel type: must be TextChannel";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    ChannelType value();

    @Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.ANNOTATION_TYPE})
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    @interface List {
        ChannelHasType[] value();
    }
}
