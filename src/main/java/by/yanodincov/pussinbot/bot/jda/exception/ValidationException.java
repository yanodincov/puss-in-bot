package by.yanodincov.pussinbot.bot.jda.exception;

public class ValidationException extends Exception {
    public ValidationException(String defaultMessage) {
        super(defaultMessage);
    }
}
