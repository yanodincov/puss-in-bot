package by.yanodincov.pussinbot.bot.jda.request.consumer.validator;

import by.yanodincov.pussinbot.bot.jda.request.RequestFactory;
import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.interactions.callbacks.IReplyCallback;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

@Component
@AllArgsConstructor
public class RequestControllerMethodValidator<E extends IReplyCallback> {
    protected RequestFactory<E> requestFactory;

    public String getValidateError(Method method, Class<?> controllerClass) {
        if (Modifier.isPrivate(method.getModifiers())) {
            return "Class %s method '%s' must be public".
                    formatted(controllerClass.getName(), method.getName());
        }
        if (method.getParameters().length > 1) {
            return "Class %s method %s have must have no more 1 arguments (now - %d)".
                    formatted(controllerClass.getName(), method.getName(), method.getParameters().length);
        }
        if (method.getParameters().length == 1) {
            var firstParameter = method.getParameters()[0];
            var parametetClass = firstParameter.getType();
            try {
                parametetClass.getDeclaredConstructor();
            } catch (NoSuchMethodException e) {
                return "Class %s method %s params %s class %s must have no args constructor".
                        formatted(controllerClass.getName(), method.getName(), firstParameter.getName(), parametetClass.getName());
            }
            this.requestFactory.validateRequestClass(method, parametetClass);
        }

        return null;
    }
}
