package by.yanodincov.pussinbot.bot.jda.request;


import by.yanodincov.pussinbot.bot.jda.request.annotation.JDARequestField;
import by.yanodincov.pussinbot.bot.jda.request.parser.provider.RequestFieldParserProvider;
import jakarta.annotation.Nonnull;
import jakarta.validation.ValidationException;
import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.interactions.callbacks.IReplyCallback;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

@Component
@AllArgsConstructor
public class RequestFactory<E extends IReplyCallback> {
    private final RequestFieldParserProvider<E> parserProvider;

    public <MethodRequest> MethodRequest fillRequest(@Nonnull Method method, @Nonnull MethodRequest request, @Nonnull E event) throws IllegalAccessException {
        for (Field field : request.getClass().getDeclaredFields()) {
            if (AnnotationUtils.findAnnotation(field, JDARequestField.class) == null) {
                continue;
            }

            var parser = this.parserProvider.getParser(method, field);
            if (parser == null) {
                throw new ValidationException("Class %s field %s has a request field annotation, but not found parser for exist annotation".formatted(request.getClass().getName(), field.getName()));
            }

            field.setAccessible(true);
            field.set(request, parser.getValue(method, field, event));
            field.setAccessible(false);
        }

        return request;
    }

    public void validateRequestClass(@Nonnull Method method, @Nonnull Class<?> requestClass) throws ValidationException {
        for (Field field : requestClass.getDeclaredFields()) {
            if (AnnotationUtils.findAnnotation(field, JDARequestField.class) == null) {
                continue;
            }
            var parser = this.parserProvider.getParser(method, field);
            try {
                parser.validateField(field);
            } catch (ValidationException e) {
                throw new ValidationException("Class %s field %s has error: %s".formatted(requestClass.getName(), field.getName(), e.getMessage()));
            }
        }
    }
}
