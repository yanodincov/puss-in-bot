package by.yanodincov.pussinbot.bot.jda.request.consumer.handler;

import by.yanodincov.pussinbot.bot.jda.exception.Exception;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDAReplySettings;
import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.interactions.InteractionHook;
import net.dv8tion.jda.api.interactions.callbacks.IReplyCallback;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.function.Consumer;

@Component
@AllArgsConstructor
public class EventResultHandler<E extends IReplyCallback> {
    private ExceptionHandler exceptionHandler;

    public void handleException(E event, java.lang.Exception e) {
        if (e instanceof Exception) {
            this.exceptionHandler.handle(event, e.getMessage());
            return;
        }
        if (e instanceof InvocationTargetException) {
            this.exceptionHandler.logAndHandle(event, ((InvocationTargetException) e).getTargetException().getMessage());
        }

        this.exceptionHandler.logAndHandle(event, "странная непридвиденная ошибка: %s".formatted(e.getMessage()));
    }

    public void handleControllerResult(E event, Method controllerMethod, String controllerResult) {
        if (controllerResult == null) {
            event.reply("пустой ответ").queue();
            return;
        }

        var replySettings = controllerMethod.getAnnotation(JDAReplySettings.class);
        if (replySettings == null) {
            event.reply(controllerResult).queue();
            return;
        }

        Consumer<InteractionHook> deleteMessageConsumer = replySettings.deleteAfterDelay() > 0 ?
                (h) -> h.deleteOriginal().queueAfter(replySettings.deleteAfterDelay(), replySettings.deleteAfterTimeUnit()) :
                null;

        event.reply(controllerResult).
                setEphemeral(replySettings.setEphemeral()).
                queue(deleteMessageConsumer);
    }
}
