package by.yanodincov.pussinbot.bot.jda.request.consumer.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.dv8tion.jda.api.interactions.callbacks.IReplyCallback;

import java.lang.reflect.Method;
import java.util.function.Consumer;

@AllArgsConstructor
@Getter
public class GenericRequestConsumerDTO<Event extends IReplyCallback> {
    private Object controllerObject;

    private Method controlerReflectionMethod;

    private Consumer<Event> consumer;
}
