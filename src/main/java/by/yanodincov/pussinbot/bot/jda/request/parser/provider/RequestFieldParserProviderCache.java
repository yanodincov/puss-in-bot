package by.yanodincov.pussinbot.bot.jda.request.parser.provider;

import by.yanodincov.pussinbot.bot.jda.request.parser.AbstractRequestFieldParser;
import lombok.NoArgsConstructor;
import net.dv8tion.jda.api.interactions.callbacks.IReplyCallback;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

@Component
@NoArgsConstructor
public class RequestFieldParserProviderCache<E extends IReplyCallback> {
    private final Map<Method, Map<Field, AbstractRequestFieldParser<E>>> parsersCache = new HashMap<>();

    public AbstractRequestFieldParser<E> getParser(Method method, Field field) {
        if (!this.parsersCache.containsKey(method)) {
            return null;
        }

        return this.parsersCache.get(method).get(field);
    }

    public AbstractRequestFieldParser<E> setParser(Method method, Field field, AbstractRequestFieldParser<E> parser) {
        if (!this.parsersCache.containsKey(method)) {
            this.parsersCache.put(method, new HashMap<>());
        }

        return this.parsersCache.get(method).put(field, parser);
    }
}
