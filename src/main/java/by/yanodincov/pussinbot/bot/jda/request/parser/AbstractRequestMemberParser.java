package by.yanodincov.pussinbot.bot.jda.request.parser;

import by.yanodincov.pussinbot.bot.jda.request.annotation.JDALongEntityID;
import jakarta.annotation.Nonnull;
import jakarta.validation.ValidationException;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.interactions.callbacks.IReplyCallback;

import java.lang.reflect.Field;

public abstract class AbstractRequestMemberParser<E extends IReplyCallback> extends AbstractRequestFieldParser<E> {
    @Override
    public void validateField(@Nonnull Field field) throws ValidationException {
        var requiredClass = field.isAnnotationPresent(JDALongEntityID.class) ? Long.class : Member.class;

        if (!field.getType().isAssignableFrom(requiredClass)) {
            throw new ValidationException("must instance of %s".formatted(requiredClass.getName()));
        }
    }
}
