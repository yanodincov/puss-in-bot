package by.yanodincov.pussinbot.bot.jda.request.consumer;

import by.yanodincov.pussinbot.bot.jda.request.annotation.JDARequestHandler;
import by.yanodincov.pussinbot.bot.jda.request.consumer.dto.GenericRequestConsumerDTO;
import by.yanodincov.pussinbot.bot.jda.request.consumer.handler.EventHandler;
import by.yanodincov.pussinbot.bot.jda.request.consumer.validator.RequestControllerMethodValidator;
import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.interactions.callbacks.IReplyCallback;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

@Component
@AllArgsConstructor
@Service
public class RequestConsumerFactory<E extends IReplyCallback> {
    private RequestControllerMethodValidator<E> controllerMethodValidator;

    private EventHandler<E> eventHandler;

    public List<GenericRequestConsumerDTO<E>> createRequestConsumers(List<Object> controllers) {
        var consumerDTOs = new ArrayList<GenericRequestConsumerDTO<E>>();

        for (Object controller : controllers) {
            var controllerClass = controller.getClass();
            for (Method method : controllerClass.getDeclaredMethods()) {
                if (AnnotationUtils.findAnnotation(method, JDARequestHandler.class) == null) {
                    continue;
                }

                var error = this.controllerMethodValidator.getValidateError(method, controllerClass);
                if (error != null) {
                    throw new IllegalStateException(error);
                }

                consumerDTOs.add(new GenericRequestConsumerDTO<>(controller, method, (event) ->
                        this.eventHandler.handleEvent(controller, method, event)));
            }
        }

        return consumerDTOs;
    }


}
