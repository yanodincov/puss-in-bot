package by.yanodincov.pussinbot.bot.jda.request.consumer.handler;

import by.yanodincov.pussinbot.bot.jda.request.consumer.factory.ConsumerRequestObjectFactory;
import jakarta.annotation.Nonnull;
import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.interactions.callbacks.IReplyCallback;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@Component
@AllArgsConstructor
public class EventHandler<E extends IReplyCallback> {
    private ConsumerRequestObjectFactory<E> requestObjectFactory;

    private EventResultHandler<E> eventResultHandler;

    public void handleEvent(@Nonnull Object controller, @Nonnull Method method, @Nonnull E event) {
        Object requestObject;
        try {
            requestObject = this.requestObjectFactory.createRequest(method, event);
        } catch (java.lang.Exception e) {
            this.eventResultHandler.handleException(event, e);
            return;
        }

        try {
            String result = this.invokeMethod(controller, method, requestObject);
            this.eventResultHandler.handleControllerResult(event, method, result);
        } catch (java.lang.Exception e) {
            this.eventResultHandler.handleException(event, e);
        }
    }

    private String invokeMethod(@Nonnull Object controller, @Nonnull Method method, Object requestObject) throws InvocationTargetException, IllegalAccessException {
        method.setAccessible(true);
        if (method.getParameters().length == 0) {
            return (String) method.invoke(controller);
        }

        return (String) method.invoke(controller, requestObject);
    }
}
