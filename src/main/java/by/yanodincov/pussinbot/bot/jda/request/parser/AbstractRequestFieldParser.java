package by.yanodincov.pussinbot.bot.jda.request.parser;

import jakarta.annotation.Nonnull;
import jakarta.validation.ValidationException;
import net.dv8tion.jda.api.interactions.callbacks.IReplyCallback;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public abstract class AbstractRequestFieldParser<E extends IReplyCallback> {
    public abstract Object getValue(@Nonnull Method method, @Nonnull Field field, @Nonnull E event);

    public abstract void validateField(@Nonnull Field field) throws ValidationException;
}
