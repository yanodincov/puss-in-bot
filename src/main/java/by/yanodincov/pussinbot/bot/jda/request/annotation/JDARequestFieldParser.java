package by.yanodincov.pussinbot.bot.jda.request.annotation;


import org.springframework.stereotype.Component;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE, ElementType.PARAMETER})
@Component
@JDARequestFieldParserQualifier
public @interface JDARequestFieldParser {
    // Must contains JDARequestHandler
    Class<? extends Annotation> requestHandlerAnnotation();

    // Must contains JDARequestField
    Class<? extends Annotation> fieldAnnotation();
}
