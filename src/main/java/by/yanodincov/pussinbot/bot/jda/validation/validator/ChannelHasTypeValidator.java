package by.yanodincov.pussinbot.bot.jda.validation.validator;

import by.yanodincov.pussinbot.bot.jda.validation.ChannelHasType;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import net.dv8tion.jda.api.entities.channel.Channel;
import net.dv8tion.jda.api.entities.channel.ChannelType;

public class ChannelHasTypeValidator implements ConstraintValidator<ChannelHasType, Channel> {
    private ChannelType channelType;

    @Override
    public void initialize(ChannelHasType constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
        this.channelType = constraintAnnotation.value();
    }

    @Override
    public boolean isValid(Channel value, ConstraintValidatorContext context) {
        return value == null || value.getType() == this.channelType;
    }
}
