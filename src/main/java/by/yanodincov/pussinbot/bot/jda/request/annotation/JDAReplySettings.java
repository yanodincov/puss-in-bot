package by.yanodincov.pussinbot.bot.jda.request.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface JDAReplySettings {
    boolean setEphemeral() default false;

    int deleteAfterDelay() default 0;

    TimeUnit deleteAfterTimeUnit() default TimeUnit.SECONDS;
}
