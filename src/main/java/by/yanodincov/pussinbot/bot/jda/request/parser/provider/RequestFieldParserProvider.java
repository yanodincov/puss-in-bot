package by.yanodincov.pussinbot.bot.jda.request.parser.provider;

import by.yanodincov.pussinbot.bot.jda.request.parser.AbstractRequestFieldParser;
import jakarta.annotation.Nonnull;
import net.dv8tion.jda.api.interactions.callbacks.IReplyCallback;
import org.springframework.core.annotation.AnnotationUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

public class RequestFieldParserProvider<E extends IReplyCallback> {
    private final Map<Class<? extends Annotation>, Map<Class<? extends Annotation>, AbstractRequestFieldParser<E>>> parsers;

    private final RequestFieldParserProviderCache<E> parsersCache;


    public RequestFieldParserProvider(Map<Class<? extends Annotation>, Map<Class<? extends Annotation>, AbstractRequestFieldParser<E>>> parsersMap) {
        this.parsers = parsersMap;
        this.parsersCache = new RequestFieldParserProviderCache<>();
    }

    public AbstractRequestFieldParser<E> getParser(@Nonnull Method requestHandlerMethod, @Nonnull Field requestField) {
        var requestFieldParser = this.parsersCache.getParser(requestHandlerMethod, requestField);
        if (requestFieldParser != null) {
            return requestFieldParser;
        }

        var methodParsers = this.getMethodAnnotationParsers(requestHandlerMethod);
        if (methodParsers == null) {
            return null;
        }

        requestFieldParser = this.getRequestFieldAnnotation(methodParsers, requestField);
        if (requestFieldParser == null) {
            return null;
        }
        this.parsersCache.setParser(requestHandlerMethod, requestField, requestFieldParser);

        return requestFieldParser;
    }

    private Map<Class<? extends Annotation>, AbstractRequestFieldParser<E>> getMethodAnnotationParsers(@Nonnull Method requestHandlerMethod) {
        return this.parsers.get(
                this.parsers.keySet().
                        stream().
                        filter(methodAnnotation -> AnnotationUtils.getAnnotation(requestHandlerMethod, methodAnnotation) != null).
                        findFirst().
                        orElse(null)
        );
    }

    private AbstractRequestFieldParser<E> getRequestFieldAnnotation(@Nonnull Map<Class<? extends Annotation>, AbstractRequestFieldParser<E>> methodParsers, @Nonnull Field requestField) {
        return methodParsers.get(
                methodParsers.keySet().
                        stream().
                        filter(requestFieldAnnotation -> AnnotationUtils.getAnnotation(requestField, requestFieldAnnotation) != null).
                        findFirst().
                        orElse(null)
        );
    }
}
