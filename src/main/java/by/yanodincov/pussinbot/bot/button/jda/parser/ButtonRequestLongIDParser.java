package by.yanodincov.pussinbot.bot.button.jda.parser;

import by.yanodincov.pussinbot.bot.button.factory.ButtonRoute;
import by.yanodincov.pussinbot.bot.button.jda.annotation.JDAButton;
import by.yanodincov.pussinbot.bot.button.jda.annotation.request.JDAButtonLongID;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDARequestFieldParser;
import by.yanodincov.pussinbot.bot.jda.request.parser.AbstractRequestFieldParser;
import jakarta.annotation.Nonnull;
import jakarta.validation.ValidationException;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Objects;

@JDARequestFieldParser(requestHandlerAnnotation = JDAButton.class, fieldAnnotation = JDAButtonLongID.class)
public class ButtonRequestLongIDParser extends AbstractRequestFieldParser<ButtonInteractionEvent> {
    @Override
    public Object getValue(@Nonnull Method method, @Nonnull Field field, @Nonnull ButtonInteractionEvent event) {
        var annotation = method.getAnnotation(JDAButton.class);

        return ButtonRoute.createIDFromButtonName(annotation.value(), Objects.requireNonNull(event.getButton().getId()));
    }


    @Override
    public void validateField(Field field) throws ValidationException {
        if (!field.getType().isAssignableFrom(Long.class)) {
            throw new ValidationException("must instance of %s".formatted(Long.class));
        }
    }
}
