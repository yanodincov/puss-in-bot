package by.yanodincov.pussinbot.bot.button.jda;

import by.yanodincov.pussinbot.bot.button.jda.annotation.JDAButtonController;
import by.yanodincov.pussinbot.bot.button.jda.listener.ButtonListenerAdapter;
import by.yanodincov.pussinbot.bot.jda.request.consumer.RequestConsumerFactory;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;
import net.dv8tion.jda.api.hooks.EventListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ButtonFactory {
    private final List<Object> controllers;
    private final RequestConsumerFactory<ButtonInteractionEvent> consumerFactory;

    @Autowired
    public ButtonFactory(
            @JDAButtonController List<Object> controllers,
            RequestConsumerFactory<ButtonInteractionEvent> consumerFactory
    ) {
        this.consumerFactory = consumerFactory;
        this.controllers = controllers;
    }

    public EventListener createListener() {
        var consumers = this.consumerFactory.createRequestConsumers(this.controllers);

        return new ButtonListenerAdapter(consumers);
    }
}
