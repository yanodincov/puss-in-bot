package by.yanodincov.pussinbot.bot.button.jda.annotation;


import by.yanodincov.pussinbot.bot.button.factory.ButtonRoute;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDARequestHandler;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@Component
@Qualifier
@JDARequestHandler
public @interface JDAButton {
    ButtonRoute.Button value();
}
