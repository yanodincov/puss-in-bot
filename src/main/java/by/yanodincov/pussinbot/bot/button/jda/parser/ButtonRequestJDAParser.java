package by.yanodincov.pussinbot.bot.button.jda.parser;

import by.yanodincov.pussinbot.bot.button.jda.annotation.JDAButton;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDAObject;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDARequestFieldParser;
import by.yanodincov.pussinbot.bot.jda.request.parser.AbstractRequestJDAParser;
import jakarta.annotation.Nonnull;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

@JDARequestFieldParser(requestHandlerAnnotation = JDAButton.class, fieldAnnotation = JDAObject.class)
public class ButtonRequestJDAParser extends AbstractRequestJDAParser<ButtonInteractionEvent> {
    @Override
    public Object getValue(@Nonnull Method method, @Nonnull Field field, @Nonnull ButtonInteractionEvent event) {
        return event.getJDA();
    }

}
