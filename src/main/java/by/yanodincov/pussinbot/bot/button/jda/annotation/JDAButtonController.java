package by.yanodincov.pussinbot.bot.button.jda.annotation;


import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@Component
@Qualifier
public @interface JDAButtonController {
}
