package by.yanodincov.pussinbot.bot.button.jda.listener;

import by.yanodincov.pussinbot.bot.button.factory.ButtonRoute;
import by.yanodincov.pussinbot.bot.button.jda.annotation.JDAButton;
import by.yanodincov.pussinbot.bot.jda.request.consumer.dto.GenericRequestConsumerDTO;
import jakarta.annotation.Nonnull;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class ButtonListenerAdapter extends ListenerAdapter implements net.dv8tion.jda.api.hooks.EventListener {
    private final Map<String, Consumer<ButtonInteractionEvent>> methods;

    private final Set<String> keys;

    public ButtonListenerAdapter(List<GenericRequestConsumerDTO<ButtonInteractionEvent>> consumers) {
        this.methods = consumers.stream().collect(Collectors.toMap(
                (consumer) -> ButtonRoute.createName(
                        consumer.getControlerReflectionMethod().getAnnotation(JDAButton.class).value()
                ),
                GenericRequestConsumerDTO<ButtonInteractionEvent>::getConsumer
        ));
        this.keys = methods.keySet();
    }

    @Override
    public void onButtonInteraction(@Nonnull ButtonInteractionEvent event) {
        var buttonName = Objects.requireNonNull(event.getButton().getId());
        var key = this.keys.stream().
                filter(buttonName::startsWith).
                findFirst().
                orElse("");

        var method = this.methods.get(key);
        if (method == null) {
            return;
        }

        method.accept(event);
    }
}
