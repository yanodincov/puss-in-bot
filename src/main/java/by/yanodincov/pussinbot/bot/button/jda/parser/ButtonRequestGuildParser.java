package by.yanodincov.pussinbot.bot.button.jda.parser;

import by.yanodincov.pussinbot.bot.button.jda.annotation.JDAButton;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDAGuild;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDALongEntityID;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDARequestFieldParser;
import by.yanodincov.pussinbot.bot.jda.request.parser.AbstractRequestGuildParser;
import jakarta.annotation.Nonnull;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

@JDARequestFieldParser(requestHandlerAnnotation = JDAButton.class, fieldAnnotation = JDAGuild.class)
public class ButtonRequestGuildParser extends AbstractRequestGuildParser<ButtonInteractionEvent> {
    @Override
    public Object getValue(@Nonnull Method method, @Nonnull Field field, @Nonnull ButtonInteractionEvent event) {
        return field.isAnnotationPresent(JDALongEntityID.class) && event.getGuild() != null ? event.getGuild().getIdLong() : event.getGuild();
    }

}
