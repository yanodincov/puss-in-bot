package by.yanodincov.pussinbot.bot.button.request;

import by.yanodincov.pussinbot.bot.button.jda.annotation.request.JDAButtonLongID;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDAGuild;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDAMember;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDAObject;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;

@Getter
@NoArgsConstructor
public class GameRequest {
    @NotNull(message = "не удается получить объект бота")
    @JDAObject
    private JDA jda;

    @NotNull(message = "не удается получить информацию о гильдии")
    @JDAGuild
    private Guild guild;

    @NotNull(message = "не удается получить информацию о твоем пользователе")
    @JDAMember
    private Member member;

    @NotNull(message = "не удается получить информацию о идентификаторе игры")
    @JDAButtonLongID
    private Long gameID;
}
