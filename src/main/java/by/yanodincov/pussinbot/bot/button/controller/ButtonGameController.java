package by.yanodincov.pussinbot.bot.button.controller;

import by.yanodincov.pussinbot.bot.button.factory.ButtonRoute;
import by.yanodincov.pussinbot.bot.button.jda.annotation.JDAButton;
import by.yanodincov.pussinbot.bot.button.jda.annotation.JDAButtonController;
import by.yanodincov.pussinbot.bot.button.request.GameRequest;
import by.yanodincov.pussinbot.bot.jda.exception.ValidationException;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDAReplySettings;
import by.yanodincov.pussinbot.service.game.GameEntryService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;

@JDAButtonController
@AllArgsConstructor
class ButtonGameController {
    private GameEntryService joinGameService;

    @JDAButton(ButtonRoute.Button.JOIN_GAME)
    @JDAReplySettings(setEphemeral = true, deleteAfterDelay = 5)
    public String joinGame(@Valid GameRequest request) throws ValidationException {
        this.joinGameService.joinGame(request);

        return ":white_check_mark: Теперь ты участник игры!";
    }

    @JDAButton(ButtonRoute.Button.LEAVE_GAME)
    @JDAReplySettings(setEphemeral = true, deleteAfterDelay = 5)
    public String leaveGame(@Valid GameRequest request) throws ValidationException {
        this.joinGameService.leaveGame(request);

        return ":white_check_mark: Ты слился с игры!";
    }
}
