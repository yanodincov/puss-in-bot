package by.yanodincov.pussinbot.bot.button.factory;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class ButtonRoute {
    public static String createName(Button button) {
        return button.getName();
    }

    public static String createName(Button button, Long id) {
        return button.getName() + id;
    }

    public static Long createIDFromButtonName(Button button, String name) {
        return Long.parseLong(name.replace(button.getName(), ""));
    }

    @Getter
    @AllArgsConstructor
    public enum Button {
        JOIN_GAME("join-game-"),
        LEAVE_GAME("leave-game-");

        private final String name;
    }
}
