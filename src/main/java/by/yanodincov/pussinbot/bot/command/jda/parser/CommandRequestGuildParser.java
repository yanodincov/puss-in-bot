package by.yanodincov.pussinbot.bot.command.jda.parser;

import by.yanodincov.pussinbot.bot.command.jda.annotation.JDACommand;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDAGuild;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDALongEntityID;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDARequestFieldParser;
import by.yanodincov.pussinbot.bot.jda.request.parser.AbstractRequestGuildParser;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

@JDARequestFieldParser(requestHandlerAnnotation = JDACommand.class, fieldAnnotation = JDAGuild.class)
public class CommandRequestGuildParser extends AbstractRequestGuildParser<SlashCommandInteractionEvent> {
    @Override
    public Object getValue(Method method, Field field, SlashCommandInteractionEvent event) {
        return field.isAnnotationPresent(JDALongEntityID.class) && event.getGuild() != null
                ? event.getGuild().getIdLong() : event.getGuild();
    }
}
