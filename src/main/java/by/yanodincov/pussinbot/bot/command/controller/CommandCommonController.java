package by.yanodincov.pussinbot.bot.command.controller;

import by.yanodincov.pussinbot.bot.command.jda.annotation.JDACommand;
import by.yanodincov.pussinbot.bot.command.jda.annotation.JDACommandController;

@JDACommandController
public class CommandCommonController {
    @JDACommand(name = "ping", description = "Проверка связи")
    public String Ping() {
        return ":ping_pong: Мяу!";
    }
}
