package by.yanodincov.pussinbot.bot.command.jda.annotation;


import by.yanodincov.pussinbot.bot.jda.request.annotation.JDARequestHandler;
import org.springframework.beans.factory.annotation.Qualifier;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Qualifier
@JDARequestHandler
public @interface JDACommand {
    boolean defaultGuildOnly = true;

    String name();

    String description() default "";

    boolean isGuildOnly() default defaultGuildOnly;
}
