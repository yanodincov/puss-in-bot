package by.yanodincov.pussinbot.bot.command.controller;

import by.yanodincov.pussinbot.bot.command.jda.annotation.JDACommand;
import by.yanodincov.pussinbot.bot.command.jda.annotation.JDACommandGroupController;
import by.yanodincov.pussinbot.bot.command.jda.annotation.JDACommandOptionPermission;
import by.yanodincov.pussinbot.bot.command.request.config.*;
import by.yanodincov.pussinbot.service.config.GuildConfigService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.Permission;

@JDACommandGroupController(
        name = "set-config",
        description = "настройки бота для серевера (только для владельца серевера)"
)
@JDACommandOptionPermission(defaultPermissions = {Permission.ADMINISTRATOR})
@AllArgsConstructor
public class CommandGuildConfigController {
    private GuildConfigService guildConfigService;

    @JDACommand(name = "control-role", description = "установить роль, которая может создавать игры")
    public String setCreateGameRole(@Valid SetRoleRequest req) {
        this.guildConfigService.updateOrCreateGuildConfig(req.getGuildID(), (config) -> {
            config.setControlRoleID(req.getRoleID());
        });

        return ":white_check_mark: Изи! Теперь пользователи <@&%d> смогут создавать игры.".
                formatted(req.getRoleID());
    }

    @JDACommand(name = "default-announce-channel", description = "установить канал для аннонсов по-умолчанию")
    public String setDefaultAnnounceChannel(@Valid SetTextChannelRequest req) {
        this.guildConfigService.updateOrCreateGuildConfig(req.getGuildID(), (config) -> {
            config.setDefaultAnnounceChannelID(req.getChannel().getIdLong());
        });

        return ":white_check_mark: Изи! Теперь аннонсы игр по-умолчанию будт происходить <#%d>".
                formatted(req.getChannel().getIdLong());
    }

    @JDACommand(name = "default-log-channel", description = "установить канал для лога игры по-умолчанию")
    public String setDefaultLogChannel(@Valid SetTextChannelRequest req) {
        this.guildConfigService.updateOrCreateGuildConfig(req.getGuildID(), (config) -> {
            config.setDefaultAnnounceChannelID(req.getChannel().getIdLong());
        });

        return ":white_check_mark: Изи! Теперь лог игр по-умолчанию будет в <#%d>".
                formatted(req.getChannel().getIdLong());
    }

    @JDACommand(name = "default-game-slots", description = "установить количество слотов в игре по-умолчанию")
    public String setDefaultGameSlots(@Valid SetGameSlotsRequest req) {
        this.guildConfigService.updateOrCreateGuildConfig(req.getGuildID(), (config) -> {
            config.setDefaultSlots(req.getSlots());
        });

        return ":white_check_mark: Изи! Теперь количество слотов в игре по умолчанию %s".
                formatted(req.getSlots());
    }

    @JDACommand(name = "default-member-role", description = "установить роль игрока по-умолчанию")
    public String setDefaultMemberRole(@Valid SetRoleRequest req) {
        this.guildConfigService.updateOrCreateGuildConfig(req.getGuildID(), (config) -> {
            config.setDefaultMemberRoleID(req.getRoleID());
        });

        return ":white_check_mark: Изи! Теперь роль пользователя по-умолчанию <@&%d>".
                formatted(req.getRoleID());
    }

    @JDACommand(name = "default-chips-set-price", description = "установить стоимость набора фишек по-умолчанию")
    public String setDefaultChipsSetPrice(@Valid SetPriceRequest req) {
        this.guildConfigService.updateOrCreateGuildConfig(req.getGuildID(), (config) -> {
            config.setDefaultChipsSetPrice(req.getSetPrice());
        });

        return ":white_check_mark: Изи! Теперь стоимость набора фишек по умолчнанию - %dр".
                formatted(req.getSetPrice());
    }

    @JDACommand(name = "default-chips-set-size", description = "установить размер набора фишек по-умолчанию")
    public String setDefaultChipsSetSize(@Valid SetSetSizeRequest req) {
        this.guildConfigService.updateOrCreateGuildConfig(req.getGuildID(), (config) -> {
            config.setDefaultChipsSetSize(req.getSetSize());
        });

        return ":white_check_mark: Изи! Теперь рамзер набора фишек по умолчнанию - %dр".
                formatted(req.getSetSize());
    }

}
