package by.yanodincov.pussinbot.bot.command.jda.listener;

import by.yanodincov.pussinbot.bot.command.jda.annotation.JDACommand;
import by.yanodincov.pussinbot.bot.command.jda.annotation.JDACommandGroupController;
import by.yanodincov.pussinbot.bot.jda.request.consumer.dto.GenericRequestConsumerDTO;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.springframework.core.annotation.AnnotationUtils;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class CommandListenerAdapter extends ListenerAdapter implements net.dv8tion.jda.api.hooks.EventListener {
    private final Map<String, Consumer<SlashCommandInteractionEvent>> methods;

    public CommandListenerAdapter(List<GenericRequestConsumerDTO<SlashCommandInteractionEvent>> consumers) {
        this.methods = consumers.stream().collect(Collectors.toMap(
                (consumer) -> {
                    var groupAnnotation = AnnotationUtils.getAnnotation(consumer.getControllerObject().getClass(), JDACommandGroupController.class);
                    var prefix = groupAnnotation != null ? groupAnnotation.name() + " " : "";

                    return prefix + consumer.getControlerReflectionMethod().getAnnotation(JDACommand.class).name();
                },
                GenericRequestConsumerDTO<SlashCommandInteractionEvent>::getConsumer)
        );
    }

    @Override
    public void onSlashCommandInteraction(SlashCommandInteractionEvent event) {
        var method = this.methods.get(event.getFullCommandName());
        if (method == null) {
            return;
        }

        method.accept(event);
    }
}
