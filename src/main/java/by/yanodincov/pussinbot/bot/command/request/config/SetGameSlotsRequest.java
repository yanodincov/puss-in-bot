package by.yanodincov.pussinbot.bot.command.request.config;

import by.yanodincov.pussinbot.bot.command.jda.annotation.request.JDACommandEventOption;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDAGuild;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDALongEntityID;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.dv8tion.jda.api.interactions.commands.OptionType;

@Getter
@NoArgsConstructor
public class SetGameSlotsRequest {
    @NotNull(message = "не могу получить информацию о серевере")
    @JDALongEntityID
    @JDAGuild()
    private Long guildID;

    @NotNull(message = "количетсво слотов не заполнено")
    @Min(value = 1, message = "слотов не может быть меньше чем 1")
    @JDACommandEventOption(type = OptionType.INTEGER, name = "slots", description = "количество слотов в игре по умолчанию", required = true)
    private Integer slots;
}
