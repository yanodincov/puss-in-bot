package by.yanodincov.pussinbot.bot.command.jda;

import by.yanodincov.pussinbot.bot.command.jda.annotation.JDACommandController;
import by.yanodincov.pussinbot.bot.command.jda.listener.CommandListenerAdapter;
import by.yanodincov.pussinbot.bot.jda.request.consumer.RequestConsumerFactory;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.EventListener;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CommandFactory {
    private final List<Object> controllers;

    private final RequestConsumerFactory<SlashCommandInteractionEvent> consumerFactory;

    private final CommandDataFactory commandDataFactory;

    public CommandFactory(
            @JDACommandController List<Object> commandControllers,
            RequestConsumerFactory<SlashCommandInteractionEvent> consumerFactory,
            CommandDataFactory commandDataFactory
    ) {
        this.controllers = commandControllers;
        this.consumerFactory = consumerFactory;
        this.commandDataFactory = commandDataFactory;
    }


    public EventListener createListener() {
        var consumers = this.consumerFactory.createRequestConsumers(this.controllers);

        return new CommandListenerAdapter(consumers);
    }

    public List<CommandData> createCommands() {
        return this.commandDataFactory.createCommandsData(this.controllers);
    }
}
