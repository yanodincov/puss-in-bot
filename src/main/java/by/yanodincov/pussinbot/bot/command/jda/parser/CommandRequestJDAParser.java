package by.yanodincov.pussinbot.bot.command.jda.parser;

import by.yanodincov.pussinbot.bot.command.jda.annotation.JDACommand;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDAObject;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDARequestFieldParser;
import by.yanodincov.pussinbot.bot.jda.request.parser.AbstractRequestJDAParser;
import jakarta.annotation.Nonnull;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

@JDARequestFieldParser(requestHandlerAnnotation = JDACommand.class, fieldAnnotation = JDAObject.class)
public class CommandRequestJDAParser extends AbstractRequestJDAParser<SlashCommandInteractionEvent> {
    @Override
    public Object getValue(@Nonnull Method method, @Nonnull Field field, @Nonnull SlashCommandInteractionEvent event) {
        return event.getJDA();
    }
}
