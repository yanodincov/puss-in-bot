package by.yanodincov.pussinbot.bot.command.jda.annotation;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@JDACommandController
public @interface JDACommandGroupController {
    String name();

    String description() default "";

    boolean isGuildOnly() default true;
}
