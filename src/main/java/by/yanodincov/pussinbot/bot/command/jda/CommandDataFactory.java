package by.yanodincov.pussinbot.bot.command.jda;

import by.yanodincov.pussinbot.bot.command.jda.annotation.JDACommand;
import by.yanodincov.pussinbot.bot.command.jda.annotation.JDACommandController;
import by.yanodincov.pussinbot.bot.command.jda.annotation.JDACommandGroupController;
import by.yanodincov.pussinbot.bot.command.jda.annotation.JDACommandOptionPermission;
import by.yanodincov.pussinbot.bot.command.jda.annotation.request.JDACommandEventOption;
import net.dv8tion.jda.api.interactions.commands.DefaultMemberPermissions;
import net.dv8tion.jda.api.interactions.commands.build.*;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class CommandDataFactory {
    public List<CommandData> createCommandsData(List<Object> commandControllers) {
        var commands = new ArrayList<CommandData>(commandControllers.size());

        for (Object controller : commandControllers) {
            var controllerClass = controller.getClass();
            if (controllerClass.isAnnotationPresent(JDACommandGroupController.class)) {
                commands.add(this.getCommandGroupData(controllerClass));
            } else if (controllerClass.isAnnotationPresent(JDACommandController.class)) {
                commands.addAll(this.getCommandData(controllerClass));
            }
        }

        return commands;
    }

    private SlashCommandData getCommandGroupData(Class<?> controller) throws IllegalArgumentException {
        var controllerAnnotation = controller.getAnnotation(JDACommandGroupController.class);
        var commandData = Commands.slash(controllerAnnotation.name(), controllerAnnotation.description());
        commandData.setGuildOnly(controllerAnnotation.isGuildOnly());

        if (controller.isAnnotationPresent(JDACommandOptionPermission.class)) {
            var permissionAnnotation = controller.getAnnotation(JDACommandOptionPermission.class);
            commandData.setDefaultPermissions(DefaultMemberPermissions.enabledFor(permissionAnnotation.defaultPermissions()));
        }

        var subcommands = new ArrayList<SubcommandData>();
        for (Method method : controller.getDeclaredMethods()) {
            var methodAnnotation = method.getAnnotation(JDACommand.class);
            if (methodAnnotation == null) {
                continue;
            }
            var subcommand = new SubcommandData(methodAnnotation.name(), methodAnnotation.description());
            if (method.getParameters().length > 0) {
                subcommand.addOptions(getOptionData(method.getParameters()[0]));
            }

            if (methodAnnotation.isGuildOnly() != JDACommand.defaultGuildOnly) {
                throw new IllegalArgumentException("Command isGuildOnly property in CommandGroupController not supported. User CommandGroupController isGuildOnly property.");
            }
            if (method.isAnnotationPresent(JDACommandOptionPermission.class)) {
                throw new IllegalArgumentException("CommandOptionPermission annotation in CommandGroupController classes supports only on class. Migrate annotation to class.");
            }

            subcommands.add(subcommand);
        }
        commandData.addSubcommands(subcommands);

        return commandData;
    }

    private List<CommandData> getCommandData(Class<?> controller) {
        var commands = new ArrayList<CommandData>();
        for (Method method : controller.getDeclaredMethods()) {
            var annotation = method.getAnnotation(JDACommand.class);
            if (annotation == null) {
                continue;
            }

            var commandData = Commands.slash(annotation.name(), annotation.description());
            commandData.setGuildOnly(annotation.isGuildOnly());

            if (method.getParameters().length > 0) {
                commandData.addOptions(getOptionData(method.getParameters()[0]));
            }

            if (method.isAnnotationPresent(JDACommandOptionPermission.class)) {
                var permissionAnnotation = method.getAnnotation(JDACommandOptionPermission.class);
                commandData.setDefaultPermissions(DefaultMemberPermissions.enabledFor(permissionAnnotation.defaultPermissions()));
            }

            commands.add(commandData);
        }

        return commands;
    }

    private Collection<OptionData> getOptionData(Parameter parameter) {
        var optionsData = new ArrayList<OptionData>();
        for (Field field : parameter.getType().getDeclaredFields()) {
            if (!field.isAnnotationPresent(JDACommandEventOption.class)) {
                continue;
            }
            var option = field.getAnnotation(JDACommandEventOption.class);
            optionsData.add(new OptionData(option.type(), option.name(), option.description(), option.required()));
        }

        return optionsData;
    }
}
