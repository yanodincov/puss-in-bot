package by.yanodincov.pussinbot.bot.command.request;

import by.yanodincov.pussinbot.bot.command.jda.annotation.request.JDACommandEventOption;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDAGuild;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDALongEntityID;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDAMember;
import by.yanodincov.pussinbot.bot.jda.validation.ChannelHasType;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.channel.Channel;
import net.dv8tion.jda.api.entities.channel.ChannelType;
import net.dv8tion.jda.api.interactions.commands.OptionType;

@Getter
@NoArgsConstructor
public class CreateGameRequest {
    @NotNull(message = "не могу получить информацию о серевере")
    @JDAGuild
    private Guild guild;

    @NotNull(message = "не могу получить информацию о твоем пользователе")
    @JDAMember
    private Member member;

    @NotNull(message = "заполни дату начала игры")
    @Pattern(regexp = "^\\d{2}\\.\\d{2}\\.\\d{4} \\d{2}:\\d{2}:\\d{2}$", message = "дата должна быть в формате 01.02.2023 23:22:21")
    @JDACommandEventOption(type = OptionType.STRING, name = "start_at", description = "дата начала игры в формате 01.02.2023 23:22:21", required = true)
    private String startAt;

    @Min(value = 1, message = "стоимость набора фишек не может быть меньше 1 рубля")
    @JDACommandEventOption(type = OptionType.INTEGER, name = "chips_set_price", description = "стоимость набора фишек в рублях")
    private Integer chipSetPrice;

    @Min(value = 1, message = "размер набора фишек не может быть меньше")
    @JDACommandEventOption(type = OptionType.INTEGER, name = "chips_set_size", description = "размер набора фишек")
    private Integer chipsSetSize;

    @JDACommandEventOption(type = OptionType.ROLE, name = "member_role", description = "роль игрока")
    @JDALongEntityID
    private Long memberRole;

    @ChannelHasType(value = ChannelType.TEXT, message = "каннал анонса игр должен быть текстовым")
    @JDACommandEventOption(type = OptionType.CHANNEL, name = "announce_channel", description = "канал анонса игр")
    private Channel announceChannel;

    @ChannelHasType(value = ChannelType.TEXT, message = "каннал лога игры должен быть текстовым")
    @JDACommandEventOption(type = OptionType.CHANNEL, name = "log_channel", description = "канал лога игры")
    private Channel logChannel;

    @Min(value = 1, message = "слотов не может быть меньше чем 1")
    @JDACommandEventOption(type = OptionType.INTEGER, name = "slots", description = "количество слотов в игре")
    private Integer slots;
}
