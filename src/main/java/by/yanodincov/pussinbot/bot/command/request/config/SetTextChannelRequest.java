package by.yanodincov.pussinbot.bot.command.request.config;

import by.yanodincov.pussinbot.bot.command.jda.annotation.request.JDACommandEventOption;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDAGuild;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDALongEntityID;
import by.yanodincov.pussinbot.bot.jda.validation.ChannelHasType;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.dv8tion.jda.api.entities.channel.Channel;
import net.dv8tion.jda.api.entities.channel.ChannelType;
import net.dv8tion.jda.api.interactions.commands.OptionType;

@Getter
@NoArgsConstructor
public class SetTextChannelRequest {
    @NotNull(message = "не могу получить информацию о серевере")
    @JDALongEntityID
    @JDAGuild()
    private Long guildID;

    @NotNull(message = "каннал не заполнен")
    @ChannelHasType(value = ChannelType.TEXT, message = "каннал должен быть текстовым")
    @JDACommandEventOption(type = OptionType.CHANNEL, name = "channel", description = "текстовый каннал", required = true)
    private Channel channel;
}
