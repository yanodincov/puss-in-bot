package by.yanodincov.pussinbot.bot.command.request.config;

import by.yanodincov.pussinbot.bot.command.jda.annotation.request.JDACommandEventOption;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDAGuild;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDALongEntityID;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.dv8tion.jda.api.interactions.commands.OptionType;

@Getter
@NoArgsConstructor
public class SetRoleRequest {
    @NotNull(message = "не могу получить информацию о серевере")
    @JDALongEntityID
    @JDAGuild()
    private Long guildID;

    @NotNull(message = "роль не заполнена")
    @JDALongEntityID
    @JDACommandEventOption(type = OptionType.ROLE, name = "role", description = "роль ползователя", required = true)
    private Long roleID;
}
