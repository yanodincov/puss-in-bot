package by.yanodincov.pussinbot.bot.command.controller;

import by.yanodincov.pussinbot.bot.command.jda.annotation.JDACommand;
import by.yanodincov.pussinbot.bot.command.jda.annotation.JDACommandGroupController;
import by.yanodincov.pussinbot.bot.command.request.CreateGameRequest;
import by.yanodincov.pussinbot.bot.jda.exception.ValidationException;
import by.yanodincov.pussinbot.service.game.GameService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;

@JDACommandGroupController(name = "game", description = "управление играми")
@AllArgsConstructor
public class CommandGameController {
    private GameService createGameService;

    @JDACommand(name = "create", description = "создать игру")
    public String createGame(@Valid CreateGameRequest req) throws ValidationException {
        var game = this.createGameService.createGame(req);

        return "Игра с кодом %s создана".formatted(game.getEmojiCode());
    }
}
