package by.yanodincov.pussinbot.bot.command.jda.annotation.request;

import by.yanodincov.pussinbot.bot.jda.request.annotation.JDARequestField;
import net.dv8tion.jda.api.interactions.commands.OptionType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@JDARequestField
public @interface JDACommandEventOption {
    String name();

    String description() default "";

    OptionType type() default OptionType.STRING;

    boolean required() default false;
}
