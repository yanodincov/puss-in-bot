package by.yanodincov.pussinbot.bot.command.jda.parser;

import by.yanodincov.pussinbot.bot.command.jda.annotation.JDACommand;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDALongEntityID;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDAMember;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDARequestFieldParser;
import by.yanodincov.pussinbot.bot.jda.request.parser.AbstractRequestMemberParser;
import jakarta.annotation.Nonnull;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

@JDARequestFieldParser(requestHandlerAnnotation = JDACommand.class, fieldAnnotation = JDAMember.class)
public class CommandRequestMemberParser extends AbstractRequestMemberParser<SlashCommandInteractionEvent> {
    @Override
    public Object getValue(@Nonnull Method method, @Nonnull Field field, @Nonnull SlashCommandInteractionEvent event) {
        return field.isAnnotationPresent(JDALongEntityID.class) && event.getMember() != null
                ? event.getMember().getIdLong() : event.getMember();
    }
}
