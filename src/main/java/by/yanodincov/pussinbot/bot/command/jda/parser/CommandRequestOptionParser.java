package by.yanodincov.pussinbot.bot.command.jda.parser;

import by.yanodincov.pussinbot.bot.command.jda.annotation.JDACommand;
import by.yanodincov.pussinbot.bot.command.jda.annotation.request.JDACommandEventOption;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDALongEntityID;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDARequestFieldParser;
import by.yanodincov.pussinbot.bot.jda.request.parser.AbstractRequestFieldParser;
import jakarta.annotation.Nonnull;
import jakarta.validation.ValidationException;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.entities.channel.Channel;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

@JDARequestFieldParser(requestHandlerAnnotation = JDACommand.class, fieldAnnotation = JDACommandEventOption.class)
public class CommandRequestOptionParser extends AbstractRequestFieldParser<SlashCommandInteractionEvent> {
    @Override
    public void validateField(@Nonnull Field field) throws ValidationException {
        var annotation = field.getAnnotation(JDACommandEventOption.class);
        var requiredType = this.getRequiredType(annotation);

        if (field.isAnnotationPresent(JDALongEntityID.class)) {
            if (!ISnowflake.class.isAssignableFrom(requiredType)) {
                throw new ValidationException("annotation EventEntityID not available for type %s".formatted(annotation.type()));
            }
            if (!field.getType().isAssignableFrom(Long.class)) {
                throw new ValidationException("annotation EventEntityID available only for field with type Long");
            }
        } else {
            if (!field.getType().isAssignableFrom(requiredType)) {
                throw new ValidationException("must instance of %s".formatted(requiredType.getName()));
            }
        }
    }

    @Override
    public Object getValue(@Nonnull Method method, @Nonnull Field field, @Nonnull SlashCommandInteractionEvent event) {
        var annotation = field.getAnnotation(JDACommandEventOption.class);
        var option = event.getOption(annotation.name());
        if (option == null) {
            return null;
        }

        Object value = switch (annotation.type()) {
            case CHANNEL -> option.getAsChannel();
            case ROLE -> option.getAsRole();
            case INTEGER, NUMBER -> option.getAsInt();
            case STRING -> option.getAsString();
            case BOOLEAN -> option.getAsBoolean();
            case USER -> option.getAsUser();
            case MENTIONABLE -> option.getAsMentionable();
            case ATTACHMENT -> option.getAsAttachment();
            default -> null;
        };

        return field.isAnnotationPresent(JDALongEntityID.class) ? (
                (value instanceof ISnowflake) ? ((ISnowflake) value).getIdLong() : null
        ) : value;
    }

    private Class<?> getRequiredType(JDACommandEventOption annotation) {
        return switch (annotation.type()) {
            case CHANNEL -> Channel.class;
            case ROLE -> Role.class;
            case INTEGER, NUMBER -> Integer.class;
            case STRING -> String.class;
            case BOOLEAN -> Boolean.class;
            case USER -> User.class;
            case MENTIONABLE -> IMentionable.class;
            case ATTACHMENT -> Message.Attachment.class;
            default -> null;
        };
    }
}
