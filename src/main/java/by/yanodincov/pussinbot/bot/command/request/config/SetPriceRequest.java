package by.yanodincov.pussinbot.bot.command.request.config;

import by.yanodincov.pussinbot.bot.command.jda.annotation.request.JDACommandEventOption;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDAGuild;
import by.yanodincov.pussinbot.bot.jda.request.annotation.JDALongEntityID;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.dv8tion.jda.api.interactions.commands.OptionType;

@Getter
@NoArgsConstructor
public class SetPriceRequest {
    @NotNull(message = "не могу получить информацию о серевере")
    @JDALongEntityID
    @JDAGuild()
    private Long guildID;

    @NotNull(message = "заполните стоимость набора фишек")
    @Min(value = 1, message = "стоимость набора фишек не может быть меньше 1 рубля")
    @JDACommandEventOption(type = OptionType.INTEGER, name = "set_price", description = "стоимость набора фишек в рублях", required = true)
    private Integer setPrice;
}
