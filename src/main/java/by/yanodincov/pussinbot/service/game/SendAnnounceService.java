package by.yanodincov.pussinbot.service.game;

import by.yanodincov.pussinbot.bot.jda.exception.ValidationException;
import by.yanodincov.pussinbot.common.factory.AnnounceMessageFactory;
import by.yanodincov.pussinbot.db.entity.Game;
import by.yanodincov.pussinbot.service.game.event.GameEventService;
import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.utils.messages.MessageEditData;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class SendAnnounceService {
    private final AnnounceMessageFactory messageFactory;

    private final GameEventService gameEventService;

    private final GameService gameService;

    public void actualizeGameAnnounceByID(JDA jda, Long gameID) throws ValidationException {
        var game = this.gameService.getGame(gameID);
        if (game == null) {
            return;
        }

        this.actualizeGameAnnounce(jda, game);
    }

    public void actualizeGameAnnounce(JDA jda, Game game) throws ValidationException {
        var channel = this.getJDATextChannel(jda, game);
        var createMessageData = this.messageFactory.createMessageData(game);

        if (game.getAnnounceMessageID() == null) {
            channel.sendMessage(createMessageData.getContent()).queue(message ->
                    this.gameService.setGameAnnounceMessageID(game.getId(), message.getIdLong()));
        } else {
            channel.editMessageById(
                    game.getAnnounceMessageID(),
                    MessageEditData.fromCreateData(createMessageData)
            ).queue();
        }
    }

    private TextChannel getJDATextChannel(JDA jda, Game game) throws ValidationException {
        var guild = jda.getGuildById(game.getGuildID());
        if (guild == null) {
            this.gameEventService.setGameIsStopped(game, true);
            throw new ValidationException("Сервер игры не найден");
        }

        var channel = guild.getTextChannelById(game.getAnnounceChannelID());
        if (channel == null) {
            this.gameEventService.setGameIsStopped(game, true);
            throw new ValidationException("Каннал игры не найден");
        }

        return channel;
    }
}

