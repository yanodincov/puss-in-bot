package by.yanodincov.pussinbot.service.game;

import by.yanodincov.pussinbot.bot.command.request.CreateGameRequest;
import by.yanodincov.pussinbot.bot.jda.exception.ValidationException;
import by.yanodincov.pussinbot.db.entity.Game;
import by.yanodincov.pussinbot.db.repository.GameRepository;
import by.yanodincov.pussinbot.service.game.event.GameEventService;
import by.yanodincov.pussinbot.service.game.factory.CreateGameDTOFactory;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class GameService {
    private final CreateGameDTOFactory gameDTOFactory;
    private final GameRepository gameRepository;

    private final GameEventService gameEventService;

    public Game createGame(CreateGameRequest req) throws ValidationException {
        var game = this.gameRepository.save(this.gameDTOFactory.createCreateGameDTO(req).getGame());
        this.gameEventService.createGameEvent(game);

        return game;
    }

    public Game getGame(Long gameID) {
        return this.gameRepository.findById(gameID).orElse(null);
    }

    public void setGameAnnounceMessageID(Long gameID, Long announceMessageID) {
        this.gameRepository.updateAnnounceMessageIDById(gameID, announceMessageID);
    }
}
