package by.yanodincov.pussinbot.service.game.factory;

import by.yanodincov.pussinbot.bot.command.request.CreateGameRequest;
import by.yanodincov.pussinbot.bot.jda.exception.ValidationException;
import by.yanodincov.pussinbot.common.factory.EmojiCodeFactory;
import by.yanodincov.pussinbot.dto.CreateGameDTO;
import by.yanodincov.pussinbot.service.config.GuildConfigService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.SmartValidator;

import java.util.Objects;

@Component
@AllArgsConstructor
public class CreateGameDTOFactory {
    private static final int emojiCodeLength = 6;
    private final GuildConfigService guildConfigService;
    private final SmartValidator validator;
    private final EmojiCodeFactory emojiCodeFactory;

    public CreateGameDTO createCreateGameDTO(CreateGameRequest req) throws ValidationException {
        var config = guildConfigService.getGuildConfig(req.getGuild().getIdLong());
        var createDTO = new CreateGameDTO(req, config, this.emojiCodeFactory.createEmojiCode(emojiCodeLength));

        var validationResult = new BeanPropertyBindingResult(createDTO, "createGameDTO");
        this.validator.validate(createDTO, validationResult);
        if (validationResult.hasFieldErrors()) {
            throw new ValidationException(Objects.requireNonNull(validationResult.getFieldError()).getDefaultMessage());
        }

        return new CreateGameDTO(req, config, this.emojiCodeFactory.createEmojiCode(emojiCodeLength));
    }
}
