package by.yanodincov.pussinbot.service.game.event;

import by.yanodincov.pussinbot.db.entity.Game;
import by.yanodincov.pussinbot.db.entity.GameEvent;
import by.yanodincov.pussinbot.db.repository.GameEventRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class GameEventService {
    private GameEventRepository repository;

    public GameEvent createGameEvent(Game game) {
        return this.repository.save(new GameEvent(game));
    }

    public void setGameIsStarted(Game game, boolean isStarted) {
        this.repository.UpdateEventIsStarted(game, isStarted);
    }

    public void setGameIsStopped(Game game, boolean isStopped) {
        this.repository.UpdateEventIsStopped(game, isStopped);
    }
}
