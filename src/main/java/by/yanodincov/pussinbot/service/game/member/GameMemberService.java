package by.yanodincov.pussinbot.service.game.member;

import by.yanodincov.pussinbot.db.entity.Game;
import by.yanodincov.pussinbot.db.entity.GameMember;
import by.yanodincov.pussinbot.db.repository.GameMemberRepository;
import by.yanodincov.pussinbot.service.member.MemberService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class GameMemberService {
    private MemberService getMemberService;

    private GameMemberRepository gameMemberRepository;

    public void createGameMember(Game game, Long memberID) {
        var member = this.getMemberService.getOrCreateMember(memberID);
        var gameMember = this.gameMemberRepository.getByGameIDAndMemberID(game.getId(), member.getId());
        if (gameMember != null) {
            return;
        }

        this.gameMemberRepository.save(GameMember.builder().
                game(game).
                member(member).
                build()
        );
    }

    public void deleteGameMember(Game game, Long memberID) {
        var member = this.getMemberService.getOrCreateMember(memberID);
        var gameMember = this.gameMemberRepository.getByGameIDAndMemberID(game.getId(), member.getId());
        if (gameMember != null) {
            this.gameMemberRepository.delete(gameMember);
        }
    }
}
