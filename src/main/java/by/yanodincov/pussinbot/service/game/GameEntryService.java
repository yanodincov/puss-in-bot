package by.yanodincov.pussinbot.service.game;

import by.yanodincov.pussinbot.bot.button.request.GameRequest;
import by.yanodincov.pussinbot.bot.jda.exception.ValidationException;
import by.yanodincov.pussinbot.db.entity.Game;
import by.yanodincov.pussinbot.db.repository.GameRepository;
import by.yanodincov.pussinbot.service.game.member.GameMemberService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class GameEntryService {
    private GameRepository repository;
    private GameMemberService gameMemberService;
    private SendAnnounceService sendAnnounceService;

    public void joinGame(GameRequest request) throws ValidationException {
        var game = this.getGame(request);
        if (game.getGameMembers().size() >= game.getSlots()) {
            throw new ValidationException("все слоты уже заняты");
        }

        if (request.getMember().getRoles().stream().noneMatch(
                r -> game.getMemberRole().equals(r.getIdLong())
        )) {
            throw new ValidationException("у тебя нет роли участника, ты не можешь играть");
        }
        this.gameMemberService.createGameMember(game, request.getMember().getIdLong());
        this.sendAnnounceService.actualizeGameAnnounceByID(request.getJda(), game.getId());
    }

    public void leaveGame(GameRequest request) throws ValidationException {
        var game = this.getGame(request);
        if (request.getMember().getRoles().stream().noneMatch(
                r -> game.getMemberRole().equals(r.getIdLong())
        )) {
            throw new ValidationException("у тебя нет роли участника, ты не можешь играть");
        }
        this.gameMemberService.deleteGameMember(game, request.getMember().getIdLong());
        this.sendAnnounceService.actualizeGameAnnounceByID(request.getJda(), game.getId());
    }

    private Game getGame(GameRequest request) throws ValidationException {
        var game = this.repository.findById(request.getGameID()).orElse(null);
        if (game == null) {
            throw new ValidationException("игра с идентификатором %d не найдена".formatted(request.getGameID()));
        }
        if (request.getGuild().getIdLong() != game.getGuildID()) {
            throw new ValidationException("эта игра не привязана к вашему серверу");
        }

        return game;
    }

}
