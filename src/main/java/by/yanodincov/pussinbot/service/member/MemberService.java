package by.yanodincov.pussinbot.service.member;

import by.yanodincov.pussinbot.db.entity.Member;
import by.yanodincov.pussinbot.db.repository.MemberRepository;
import jakarta.annotation.Nonnull;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MemberService {
    private MemberRepository repository;

    public Member getOrCreateMember(@Nonnull Long memberID) {
        return repository.findById(memberID).orElseGet(() -> repository.save(new Member(memberID)));
    }
}
