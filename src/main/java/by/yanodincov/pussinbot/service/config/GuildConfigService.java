package by.yanodincov.pussinbot.service.config;

import by.yanodincov.pussinbot.db.entity.GuildConfig;
import by.yanodincov.pussinbot.db.repository.GuildConfigRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.function.Consumer;

@Component
@AllArgsConstructor
public class GuildConfigService {
    private final GuildConfigRepository repository;

    public void updateOrCreateGuildConfig(Long guildID, Consumer<GuildConfig> updater) {
        var config = this.repository.findById(guildID).
                orElse(new GuildConfig(guildID));

        updater.accept(config);
        this.repository.save(config);
    }

    public GuildConfig getGuildConfig(Long guildID) {
        return this.repository.findById(guildID).orElse(new GuildConfig(guildID));
    }
}
