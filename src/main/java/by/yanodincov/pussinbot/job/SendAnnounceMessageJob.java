package by.yanodincov.pussinbot.job;

import by.yanodincov.pussinbot.bot.jda.exception.ValidationException;
import by.yanodincov.pussinbot.db.repository.GameRepository;
import by.yanodincov.pussinbot.service.game.SendAnnounceService;
import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.JDA;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class SendAnnounceMessageJob {
    private static final Logger logger = LoggerFactory.getLogger(SendAnnounceMessageJob.class);

    private final JDA jda;
    private final GameRepository gameRepository;
    private final SendAnnounceService sendAnnounceService;

    @Scheduled(fixedDelay = 5000)
    public void sendGameAnnounce() {
        this.gameRepository.findByAnnounceMessageIDNullAndEvent_IsStoppedFalse().
                forEach(g -> {
                    try {
                        this.sendAnnounceService.actualizeGameAnnounce(this.jda, g);
                    } catch (ValidationException e) {
                        logger.error("send announce message error", e);
                    }
                });
    }
}
