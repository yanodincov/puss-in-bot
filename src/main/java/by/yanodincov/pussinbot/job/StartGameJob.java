package by.yanodincov.pussinbot.job;

import by.yanodincov.pussinbot.bot.jda.exception.ValidationException;
import by.yanodincov.pussinbot.db.repository.GameRepository;
import by.yanodincov.pussinbot.service.game.SendAnnounceService;
import by.yanodincov.pussinbot.service.game.event.GameEventService;
import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.JDA;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;

@Component
@AllArgsConstructor
public class StartGameJob {
    private static final Logger logger = LoggerFactory.getLogger(StartGameJob.class);
    private final JDA jda;
    private final GameRepository gameRepository;

    private final GameEventService gameEventService;
    private final SendAnnounceService sendAnnounceService;

    @Scheduled(fixedDelay = 5000)
    public void sendGameAnnounce() {
        var games = this.gameRepository.findByEvent_IsStartedFalseAndStartAtLessThan(ZonedDateTime.now());
        games.forEach(g -> {
            try {
                this.gameEventService.setGameIsStarted(g, true);
                this.sendAnnounceService.actualizeGameAnnounceByID(this.jda, g.getId());
            } catch (ValidationException e) {
                logger.error("send announce message error", e);
            }
        });
    }
}
