package by.yanodincov.pussinbot.dto;

import by.yanodincov.pussinbot.bot.command.request.CreateGameRequest;
import by.yanodincov.pussinbot.common.converter.ISnowflakeIDProvider;
import by.yanodincov.pussinbot.db.entity.Game;
import by.yanodincov.pussinbot.db.entity.GuildConfig;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import org.apache.commons.lang3.ObjectUtils;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

@Getter
public class CreateGameDTO {
    private static final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss").withZone(TimeZone.getDefault().toZoneId());
    @NotNull(message = "не могу получить информацию о серевере")
    private final Long guildID;
    @NotNull(message = "заполните emoji code")
    private final String emojiCode;
    @NotNull(message = "заполни дату начала игры")
    private final ZonedDateTime startAt;
    @NotNull(message = "укажи роль игрока")
    private final Long memberRoleID;
    @NotNull(message = "заполни канал для аннонса игр")
    private final Long announceChannelID;
    @NotNull(message = "заполни канал для лога игры")
    private final Long logChannelID;
    @NotNull(message = "заполни стоимость набора фишек")
    @Min(value = 1, message = "стоимость набора фишек не может быть меньше 1 рубля")
    private final Integer chipSetPrice;
    @NotNull(message = "заполни размер набора фишек")
    @Min(value = 1, message = "размер набора фишек не может быть меньше 1")
    private final Integer chipSetSize;
    @NotNull(message = "заполни количество слотов в игре")
    @Min(value = 1, message = "слотов не может быть меньше чем 1")
    private final Integer slots;

    public CreateGameDTO(CreateGameRequest request, GuildConfig config, String emojiCode) {
        this.guildID = request.getGuild().getIdLong();
        this.startAt = ZonedDateTime.parse(request.getStartAt(), timeFormatter);
        this.emojiCode = emojiCode;
        this.chipSetPrice = ObjectUtils.firstNonNull(request.getChipSetPrice(), config.getDefaultChipsSetPrice());
        this.chipSetSize = ObjectUtils.firstNonNull(request.getChipsSetSize(), config.getDefaultChipsSetPrice());
        this.memberRoleID = ObjectUtils.firstNonNull(request.getMemberRole(), config.getDefaultLogChannelID());
        this.announceChannelID = ObjectUtils.firstNonNull(ISnowflakeIDProvider.getID(request.getAnnounceChannel()), config.getDefaultAnnounceChannelID());
        this.logChannelID = ObjectUtils.firstNonNull(ISnowflakeIDProvider.getID(request.getLogChannel()), config.getDefaultAnnounceChannelID());
        this.slots = ObjectUtils.firstNonNull(request.getSlots(), config.getDefaultSlots());
    }

    public Game getGame() {
        return Game.builder().createdAt(ZonedDateTime.now()).emojiCode(this.emojiCode).guildID(this.getGuildID()).startAt(this.getStartAt()).memberRole(this.getMemberRoleID()).announceChannelID(this.getAnnounceChannelID()).logChannelID(this.getLogChannelID()).slots(this.getSlots()).chipsSetPrice(this.getChipSetPrice()).chipsSetSize(this.getChipSetSize()).build();
    }
}
